var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/get', function (req, res) {
  res.send('Get request to the homepage');
  console.log('Test get');
})

module.exports = router;
