# Crud

CRUD Node Js - Express Js

* Create 2 microseservices (nodejs – Expressjs)
* for CRUD operation and store in database (mongodb) for text notes (Title and content) (1 user can has multiple notes)
* for CRUD operation for user login data in database (mongodb)
* Login User (Email and Password) will return JWT Token
* Protect the API with the authorization header
* Validate the header request (authorization header)
* Deploy into docker/heroku or any platform services.

Notes:

* Provide API to generate the token
* Provide GQL for any operation CRUD and login
* Use your name for database and microservices name.
