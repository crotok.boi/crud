const Content = require('../models/content.model');

//Simple version, without validation or sanitation
exports.test = function (req, res) {
    res.send('Greetings from the Test controller!');
};

exports.content_details = function (req, res) {
    Content.findById(req.params.id, function (err, content) {
        if (err) return next(err);
        res.send(content);
    }).populate('user').select('content user')
};

exports.content_by_user = function (req, res){
   
    Content.find({user:req.params.user}, function(err, contents){
        if (err) return res.send(err);
        res.send(contents);
    });
}

exports.content_create = function (req, res) {
    let content = new Content(
        {
            title: req.body.title,
            content: req.body.content,
            user: req.body.user
        }
    );

    content.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send(content)
    })
};

exports.content_update = function (req, res) {
    Content.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, content) {
        if (err) return next(err);
        res.send(content);
    });
};

exports.content_delete = function (req, res) {
    Content.findByIdAndRemove(req.params.id, function (err) {
        if (err) return next(err);
        res.send('Deleted successfully!');
    })
};