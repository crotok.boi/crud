const User = require('../models/user.model');

exports.test = function (req, res) {
    let user = new User(
        {
            email: 'email sample',
            password: 'password sample'
        }
    );
    res.send(user);
};

exports.user_details = function (req, res) {
    User.findById(req.params.id, function (err, user) {
        if (err) return res.send(err);
        res.send(user);
    })
};

exports.user_create = function (req, res) {
    let user = new User(
        {
            email: req.body.email,
            password: req.body.password
        }
    );

    user.save(function (err) {
        if (err) {
            return  res.send(err);
        }
        res.send(user)
    })
};

exports.user_update = function (req, res) {
    User.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, user) {
        if (err) return  res.send(err);
        res.send(user);
    });
};

exports.user_delete = function (req, res) {
    User.findByIdAndRemove(req.params.id, function (err) {
        if (err) return  res.send(err);
        res.send('Deleted successfully!');
    })
};