const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ContentSchema = new Schema({
    title: {type: String, required: true, max: 100},
    content: {type: String, required: true},
    user: {type:Schema.Types.ObjectId, ref:'user'}
});


// Export the model
module.exports = mongoose.model('content', ContentSchema);